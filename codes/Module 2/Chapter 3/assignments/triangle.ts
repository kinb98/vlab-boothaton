var p1 : HTMLInputElement = <HTMLInputElement>document.getElementById("p1");//
var p2 : HTMLInputElement = <HTMLInputElement>document.getElementById("p2");
var p3 : HTMLInputElement = <HTMLInputElement>document.getElementById("p3");
var p4 : HTMLInputElement = <HTMLInputElement>document.getElementById("p4");
//getting the tag for the final result
var result : HTMLParagraphElement = <HTMLParagraphElement>(document.getElementById("result"));


// function used to return the area of a triangle with given coordinates
function calcArea( x1: number, y1: number,x2: number,y2: number,x3: number,y3: number){
    var area: number = Math.abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2);
    return area;
}


//function used to return point p in the form of an array
function conv(p){
    let res = []
    let arr = p.split(',');
    for(let i=0; i < p.length; i++)
        res.push(parseInt(arr[i]));
    return res;
}


//function used to check whether the given point lies inside the triangle or not
function check(){
var [x1,y1] = conv(p1.value);
var [x2,y2] = conv(p2.value);
var [x3,y3] = conv(p3.value);
var [x4,y4] = conv(p4.value);


//checking if the input is a valid integer or not
if (isNaN(x1) || isNaN(y1) || isNaN(x2) || isNaN(y2) || isNaN(x3) || isNaN(y3) || isNaN(x4) || isNaN(y4)){
    result.innerHTML = "Input is not an integer";
}
else{
    var areaOfTriangle = calcArea(x1,y1,x2,y2,x3,y3); //area of main triangle
    var area1 = calcArea(x2,y2,x3,y3,x4,y4);// area of sub triangle 1
    var area2 = calcArea(x1,y1,x3,y3,x4,y4);// area of sub triangle 2
    var area3 = calcArea(x1,y1,x2,y2,x4,y4);// area of sub triangle 3

    if(Math.abs(areaOfTriangle - (area1 + area2 + area3)) < 0.001){
        result.innerHTML = "The point is inside the triangle";
    } 
    else{
        result.innerHTML = "The point is outside the triangle";
    }
}
}