var p1 = document.getElementById("p1");
var p2 = document.getElementById("p2");
var p3 = document.getElementById("p3");
var p4 = document.getElementById("p4");
var result = (document.getElementById("result"));
function calcArea(x1, y1, x2, y2, x3, y3) {
    var area = Math.abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2);
    return area;
}
function conv(p) {
    let res = [];
    let arr = p.split(',');
    for (let i = 0; i < p.length; i++)
        res.push(parseInt(arr[i]));
    return res;
}
function check() {
    var [x1, y1] = conv(p1.value);
    var [x2, y2] = conv(p2.value);
    var [x3, y3] = conv(p3.value);
    var [x4, y4] = conv(p4.value);
    if (isNaN(x1) || isNaN(y1) || isNaN(x2) || isNaN(y2) || isNaN(x3) || isNaN(y3) || isNaN(x4) || isNaN(y4)) {
        result.innerHTML = "Input is not an integer";
    }
    else {
        var areaOfTriangle = calcArea(x1, y1, x2, y2, x3, y3);
        var area1 = calcArea(x2, y2, x3, y3, x4, y4);
        var area2 = calcArea(x1, y1, x3, y3, x4, y4);
        var area3 = calcArea(x1, y1, x2, y2, x4, y4);
        if (areaOfTriangle == area1 + area2 + area3) {
            result.innerHTML = "The point is inside the triangle";
        }
        else {
            result.innerHTML = "The point is outside the triangle";
        }
    }
}
//# sourceMappingURL=triangle.js.map