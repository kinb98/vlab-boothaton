var num: HTMLInputElement = < HTMLInputElement > document.getElementById("num"); //getting the number
var table: HTMLTableElement = < HTMLTableElement > document.getElementById("tbl"); //getting the table tag

function pr() {
    var n = parseInt(num.value);
    //checking if the input is valid or not
    if (isNaN(n) || n < 1) {
        alert("Please enter a valid number");
    } else {
        //Resetting the table so as to clear all the result before any query.
        table.innerHTML = "";
        for (var i = 1; i <= n; i++) {
            // loop to create rows and adding elements to it in order to create table
            var r: HTMLTableRowElement = table.insertRow();
            var tc: HTMLTableCellElement = r.insertCell();
            var t: HTMLParagraphElement = document.createElement("p");
            t.innerHTML = n.toString();
            tc.append(t);
            tc = r.insertCell();
            t = document.createElement("p");
            t.innerHTML = "*";
            tc.appendChild(t);
            tc = r.insertCell();
            t = document.createElement("p");
            t.innerHTML = i.toString();
            tc.appendChild(t);
            tc = r.insertCell();
            t = document.createElement("p");
            t.innerHTML = "=";
            tc.appendChild(t);
            tc = r.insertCell();
            t = document.createElement("p");
            t.innerHTML = (i * n).toString();
            tc.appendChild(t);
        }
    }
}