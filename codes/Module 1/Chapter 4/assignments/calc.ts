let num1 : HTMLInputElement = <HTMLInputElement>document.getElementById("n1");
let num2 : HTMLInputElement = <HTMLInputElement>document.getElementById("n2");
let res : HTMLInputElement = <HTMLInputElement>document.getElementById("res");

function add(){
    var c:number = parseFloat(num1.value) + parseFloat(num2.value);
    res.value = c.toString();
}

function subtract(){
    var c:number = parseFloat(num1.value) - parseFloat(num2.value);
    res.value = c.toString();
}

function multiply(){
    var c:number = parseFloat(num1.value) * parseFloat(num2.value);
    res.value = c.toString();
}

function divide(){
    var c:number = parseFloat(num1.value) / parseFloat(num2.value);
    res.value = c.toString();
}