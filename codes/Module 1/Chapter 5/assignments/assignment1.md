# Context Free Grammar

## Theory 

A Context Free Grammar is a set of productions used for generating sentences of a language.

Context Free Grammar has four components:
1. A set of **terminal symbols**, which are lexicons or words.
2. A set of **non-terminal symbols**, which are generally denoted by phrases in computational linguistics. Pre-terminal is the special set of non-terminals which always extend to terminals. Pre-terminals are nothing but Parts of Speech.
3. A set of **productions or rules** of the form V -> w, where V is a non-terminal and w is a sequence of terminals and non-terminals.
4. A **start symbol**, a special non-terminal symbol which appears initially and stands for sentence.
   
**Example :-**

Rules -

- S -> NP VP
- NP -> det noun
- NP -> noun
- NP -> det adj noun
- NP -> adj noun
- VP -> verb NP

**Sentence** : *Ram eats a mango*

*Now, we will show how this sentence is formed using the rules*

1. S -> NP VP
2. S -> noun VP
3. S -> noun verb NP
4. S -> noun verb det noun
5. S -> Ram eats a mango *(RHS in above step is nothing but the Parts of Speech of the words in a sentence)*

However, this can also be represented in the form of phrase structure tree, as shown here:

![Diagram](PST.jpeg)

This can also be interpretated as:

**a** and mango combines to form **NP**
<br> **eats** and a mango combines to form **VP**<br>**Ram** is extended to **noun** and then to **NP**<br>**Ram** and **eats a mango** which is actually VP (from STEP 2) combines to form **S**<br>.

This is the task which is to be done in this experiment, and corresponding tree will be shown. For this experiment, you should know the parts of speech of the words

**Context Free Grammar for this experiment**

- S -> NP VP
- S -> VP
- VP -> VP PP
- VP -> VP NP
- VP -> verb NP
- VP -> aux-verb verb
- NP -> NP PP
- NP -> noun
- NP -> det noun
- NP -> det adjective noun
- NP -> adjective noun
- NP -> pronoun
